# Mergesort
def merge(array_a, array_b)
  result = []
  i = 0

  while i < array_a.size and i < array_b.size do
    array_a[i] > array_b[i] ? result.push(array_b.shift) : result.push(array_a.shift)
    i += 1
  end

  array_a.each do |e|
    result.push e
  end

  array_b.each do |e|
    result.push e
  end
  puts "result: #{ result }"
  result
end

def mergesort(array)
  return array if array.size == 1
  array_a = array[0..array.size / 2 - 1]
  array_b = array[array.size / 2..-1]

  puts "array a: #{ array_a }"
  puts "array b: #{ array_b }"

  merge mergesort(array_a), mergesort(array_b)
end
