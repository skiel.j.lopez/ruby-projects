require_relative '../bin/mergesort'

describe '#quiksort' do
  it 'array of 1000 elements, returns same array sorted' do
    sorted_array = (0..1000).to_a
    test_array   = sorted_array.shuffle
    expect(quicksort test_array).to eq sorted_array
  end

  it 'array of 1000000 elements, returns same array sorted' do
    sorted_array = (0..1000000).to_a
    test_array   = sorted_array.shuffle
    expect(quicksort test_array).to eq sorted_array
  end
end
