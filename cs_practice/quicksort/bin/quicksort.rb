# Quicksort Lomuto implementation
def quicksort(array)
  pivot = [array[-1]]
  lower = []
  upper = []

  if array.size > 1
    array.each do |e|
      lower.push e if e < pivot[0]
      upper.push e if e > pivot[0]
    end
  else
    return array
  end
  quicksort(lower) + pivot + quicksort(upper)
end
