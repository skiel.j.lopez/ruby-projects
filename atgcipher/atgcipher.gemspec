Gem::Specification.new do |s|
    s.name = 'rcipher'
    s.version = '0.0.2'
    s.date = '2016-10-30'
    s.summary = 'rcipher'
    s.description = 'ruby cipher to encrypt and decrypt text'
    s.authors = ['Ezequiel Lopez']
    s.email = ['skiel.j.lopez@gmail.com']
    s.files = ['lib/rcipher.rb', 'lib/rgpg.rb']
    s.requirements = ['gpgme', 'openssl']
    s.license = 'MIT'
end
