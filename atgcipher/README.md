# Profile extracts

A ruby gem to use encrypt and decrypt using AES-256-ECB with no padding.

installation
---

```
$ git clone git@bitbucket.org:TS24/atgcipher.git
$ cd atgcipher
$ gem build atgcipher.gemspec
$ gem install ./atgcipher-0.0.1.gem
```

Version History
---

v0.0.2 - NOVEMBER 10, 2016

    * Added GPGCrypto a class to encrypt and decrypt files using gpg

---

v0.0.1 - NOVEMBER 02, 2016

    * Initial version 
