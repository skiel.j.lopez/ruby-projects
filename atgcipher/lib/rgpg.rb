require 'gpgme'
require 'pry'

class GPGCrypto
  attr_accessor :crypto

  include GPGME

  #def self.encrypt_file(in_file, recipient, **options)
  #  in_file.seek(0)
  #  crypto = GPGME::Ctx.new options
  #  dec_data = GPGME::Data.new in_file
  #  out_put = File.open(in_file.path + '.gpg', 'w+')
  #  out_put << crypto.encrypt(recipient, dec_data)
  #end

  def self.enc_file(in_file, key_path, **options)
    GPGME::Key.import(File.open(key_path))
    crypto = GPGME::Crypto.new
    in_file.seek(0)
    options[:output] = File.open(in_file.path+'.gpg', 'w+')
    crypto.encrypt in_file, options
    options[:output]
  end

  def self.dec_file(in_file, key_path, **options)
    GPGME::Key.import(File.open(key_path))
    crypto = GPGME::Crypto.new
    in_file.seek(0)
    options[:output] = File.open(in_file.path.gsub(/\.gpg$|\.pgp$/, ''), 'w+')
    crypto.decrypt in_file, options
    options[:output]
  end

  #def self.decrypt_file(in_file, **options)
  #  in_file.seek(0)
  #  crypto = GPGME::Ctx.new(options)
  #  enc_data = GPGME::Data.new in_file
  #  out_put = File.open(in_file.path.gsub(/\.gpg$|\.pgp$/, ''), 'w+')
  #  out_put << crypto.decrypt(enc_data)
  #end
end
