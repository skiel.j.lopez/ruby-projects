require 'openssl'

class AES256Cipher
  def initialize(key)
    @key = key
  end

  def add_pad(str)
    str.encode('utf-8') + "\x00" * (16 - str.size % 16)
  end

  def rm_pad(str)
    str.gsub("\x00", '')
  end

  def encrypt(in_str)
    cipher = OpenSSL::Cipher.new('AES-256-ECB')
    cipher.padding = 0
    cipher.encrypt
    cipher.key = @key

    pad_data = self.add_pad in_str
    encrypted = (cipher.update(pad_data) + cipher.final).unpack('H*')[0]
  end

  def decrypt(in_str)
    decipher = OpenSSL::Cipher.new('AES-256-ECB')
    decipher.padding = 0
    decipher.decrypt
    decipher.key = @key

    plain = rm_pad(decipher.update([in_str].pack('H*')) + decipher.final)
  end
end
