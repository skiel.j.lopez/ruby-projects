str  = '49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d'
str1 = '1c0111001f010100061a024b53535009181c'
str2 = '686974207468652062756c6c277320657965'
message = '1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736'
message2 = 'ETAOIN'
message3 = '727f767675'

def hex_to_base64(str)
  [[str].pack("H*")].pack("m0")
end

def xor(str1, str2)
  str1_b = [str1].pack('H*').unpack('B*').first
  str2_b = [str2].pack('H*').unpack('B*').first

  result = ''
  str1_b.size.times { |i| result << (str1_b[i].to_i ^ str2_b[i].to_i).to_s }

  [result].pack('B*').unpack('H*')
end

def xor_one_byte(message)
  keys = (0..255).to_a.map { |n| [n].pack('c*').unpack('H*').first }
  result = {}
  message.split.map do |word|
    hex_word = word.bytes.map { |n| [n].pack('c*').unpack('H*').first }
    keys.each do |key|
      result[key] = hex_word.map { |l| xor(l, key).to_a.pack('H*') }.join
    end
  end
  result
end

puts hex_to_base64 str
puts xor(str1, str2).to_a.pack('H*')
xor_one_byte(message3).each { |k,v| puts "#{k}: #{v}" }
